// Fill out your copyright notice in the Description page of Project Settings.


#include "MunitionStore.h"

UMunitionStore::UMunitionStore(const class FObjectInitializer& ObjectInitializer) 
{

}

EMunitionType UMunitionStore::GetMunitionType()
{
	return UMunitionStore::MunitionType;
}

float UMunitionStore::GetCurrentAmount() const
{
	return CurrentAmount;
}

void UMunitionStore::SetCurrentAmount(const float Value)
{
	CurrentAmount += Value;
}
