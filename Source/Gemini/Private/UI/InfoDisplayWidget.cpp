// Fill out your copyright notice in the Description page of Project Settings.


#include "InfoDisplayWidget.h"
#include "Components/TextBlock.h"
#include "GameFramework/Actor.h"
#include "Kismet/KismetMathLibrary.h"

UInfoDisplayWidget::UInfoDisplayWidget(const FObjectInitializer& ObjectInitializer):UUserWidget(ObjectInitializer)
{


}

void UInfoDisplayWidget::SetInfoText(FText Value)
{
	TextInfo = Value;
}

//FVector UInfoDisplayWidget::GetTextDisplayLocation()
//{
//	if (InfoActor)
//	{
//		return UKismetMathLibrary::Add_VectorVector(InfoActor->GetActorLocation(), TextOffset);
//	}
//	return FVector(0,0,0);
//}

AActor* UInfoDisplayWidget::GetInfoActor()
{
	if (InfoActor)
	{
		return InfoActor;
	}
	return NULL;
}

//void UInfoDisplayWidget::DisplayInfo(APlayerController* Value)
//{
//	//FVector WorldLocation;
//	//if (Value->ProjectWorldLocationToScreen(Value->ActorToWorld(),Value.getve))
//	//{
//
//	//}
//}




