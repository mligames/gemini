// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MechCharacter.h"
#include "GeminiPlayerController.generated.h"


class AMechCharacter;
/**
 * 
 */
UCLASS()
class GEMINI_API AGeminiPlayerController : public APlayerController
{
	GENERATED_BODY()
public:

	AGeminiPlayerController(const class FObjectInitializer& ObjectInitializer);
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	/** sets up input */
	virtual void SetupInputComponent() override;
protected:

	virtual void OnPossess(class APawn* InPawn) override;
	virtual void OnUnPossess() override;


protected:
	/** Returns a pointer to the players pawn */
	AMechCharacter* MechPawn;
public:
	/** get currently possessed pawn */
	UFUNCTION(BlueprintCallable, Category = "MechPawn")
	AMechCharacter* GetMechPawn() const;

	/******************************************/
	/* UI and Menu                            */
	/******************************************/
	//protected:
	//	/** shooter in-game menu */
	//	TSharedPtr<class FShooterIngameMenu> ShooterIngameMenu;
//public:
//	/** Returns a pointer to the shooter game hud. May return NULL. */
//	APlayerPawnHUD* GetPlayerPawnHUD() const;
//	/** Is pause menu currently active? */
//	bool IsPauseMenuVisible() const;
//
//	/** Toggle PauseMenu handler */
//	void OnTogglePauseMenu();
//
//	/** check if gameplay related actions (movement, weapon usage, etc) are allowed right now */
//	bool IsGameInputAllowed() const;
//
//	/** is game menu currently active? */
//	bool IsGameMenuVisible() const;
//
//	virtual bool SetPause(bool bPause, FCanUnpause CanUnpauseDelegate = FCanUnpause()) override;
};
