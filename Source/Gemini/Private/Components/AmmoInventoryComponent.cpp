// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoInventoryComponent.h"
#include "MunitionStore.h"
#include "..\..\Public\Components\AmmoInventoryComponent.h"

// Sets default values for this component's properties
UAmmoInventoryComponent::UAmmoInventoryComponent(const class FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	InitMunitionStorage();

}


// Called when the game starts
void UAmmoInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UAmmoInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UAmmoInventoryComponent::InitMunitionStorage()
{
	UMunitionStore* SmallProjectile = CreateDefaultSubobject<UMunitionStore>(TEXT("SmallProjectile"));
	SmallProjectile->MunitionType = EMunitionType::Projectile_Small;
	MunitionMap.Add((int)SmallProjectile->MunitionType, SmallProjectile);

	UMunitionStore* MediumProjectile = CreateDefaultSubobject<UMunitionStore>(TEXT("MediumProjectile"));
	MediumProjectile->MunitionType = EMunitionType::Projectile_Medium;
	MunitionMap.Add((int)MediumProjectile->MunitionType, MediumProjectile);

	UMunitionStore* LargeProjectile = CreateDefaultSubobject<UMunitionStore>(TEXT("LargeProjectile"));
	LargeProjectile->MunitionType = EMunitionType::Projectile_Large;
	MunitionMap.Add((int)LargeProjectile->MunitionType, LargeProjectile);

	UMunitionStore* Accelerant = CreateDefaultSubobject<UMunitionStore>(TEXT("Accelerant"));
	Accelerant->MunitionType = EMunitionType::Accelerant;
	MunitionMap.Add((int)Accelerant->MunitionType, Accelerant);

	UMunitionStore* Capacitor = CreateDefaultSubobject<UMunitionStore>(TEXT("Capacitor"));
	Capacitor->MunitionType = EMunitionType::Capacitor;
	MunitionMap.Add((int)Capacitor->MunitionType, Capacitor);

	UMunitionStore* Explosive = CreateDefaultSubobject<UMunitionStore>(TEXT("Explosive"));
	Explosive->MunitionType = EMunitionType::Explosive;
	MunitionMap.Add((int)Explosive->MunitionType, Explosive);

	UMunitionStore* Battery = CreateDefaultSubobject<UMunitionStore>(TEXT("Battery"));
	Battery->MunitionType = EMunitionType::Battery;
	MunitionMap.Add((int)Battery->MunitionType, Battery);

	UMunitionStore* Atomic = CreateDefaultSubobject<UMunitionStore>(TEXT("Atomic"));
	Atomic->MunitionType = EMunitionType::Atomic;
	MunitionMap.Add((int)Atomic->MunitionType, Atomic);

	UMunitionStore* PlasmaCore = CreateDefaultSubobject<UMunitionStore>(TEXT("PlasmaCore"));
	PlasmaCore->MunitionType = EMunitionType::PlasmaCore;
	MunitionMap.Add((int)PlasmaCore->MunitionType, PlasmaCore);
}

void UAmmoInventoryComponent::AddMunitionType(int munitionType, int amount)
{

}

bool UAmmoInventoryComponent::TryGetMunition(int munitionType, int amount)
{
	if (MunitionMap.Contains(munitionType))
	{
		UMunitionStore* munitionStore = MunitionMap.FindRef(munitionType);
		if (munitionStore->GetCurrentAmount() >= amount)
		{

		}

		return true;
	}
	return false;
}

int UAmmoInventoryComponent::GetRemainingCapacity()
{
	return MaxCapacity - CurrentCapacity;
}



