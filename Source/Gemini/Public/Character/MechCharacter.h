// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MechCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class UMechDataComponent;
class UArrowComponent;
class UStaticMeshComponent;
class ABaseWeapon;
class UAmmoInventoryComponent;

/**
 * 
 */
UCLASS()
class GEMINI_API AMechCharacter : public ACharacter
{
	GENERATED_BODY()
public:
	// Sets default values for this character's properties
	AMechCharacter(const FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	static const FName MoveForwardBinding;
	static const FName MoveRightBinding;
	static const FName LookForwardBinding;
	static const FName LookRightBinding;
/************************************************************************/
/* Mech Components                                                      */
/************************************************************************/
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MechComponents", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* MechBodyMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MechComponents", meta = (AllowPrivateAccess = "true"))
	UMechDataComponent* MechDataComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MechComponents", meta = (AllowPrivateAccess = "true"))
	UAmmoInventoryComponent* AmmoInventoryComp;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "MechComponents")
	FName MechBodyMountSocket;
public:
	FORCEINLINE class UStaticMeshComponent* GetMechBodyMesh() const { return MechBodyMesh; }

	FORCEINLINE class UMechDataComponent* GetMechDataComp() const { return MechDataComp; }

	FORCEINLINE class UAmmoInventoryComponent* GetAmmoInventoryComp() const { return AmmoInventoryComp; }
/************************************************************************/
/* Weapons                                                              */
/************************************************************************/
protected:
	UPROPERTY(EditDefaultsOnly, Category = "MechComponents")
	TSubclassOf<ABaseWeapon> SelectedWeaponLeft;

	UPROPERTY(EditDefaultsOnly, Category = "MechComponents")
	FName WeaponLeftSocket;

	ABaseWeapon* WeaponLeft;

	FORCEINLINE class ABaseWeapon* GetWeaponLeft() const { return WeaponLeft; }

public:
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void FireWeaponLeft();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void StopFireWeaponLeft();

protected:
	UPROPERTY(EditDefaultsOnly, Category = "MechComponents")
	TSubclassOf<ABaseWeapon> SelectedWeaponRight;

	UPROPERTY(EditDefaultsOnly, Category = "MechComponents")
	FName WeaponRightSocket;

	ABaseWeapon* WeaponRight;

	FORCEINLINE class ABaseWeapon* GetWeaponRight() const { return WeaponRight; }

public:
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void FireWeaponRight();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void StopFireWeaponRight();
/************************************************************************/
/* Camera                                                               */
/************************************************************************/
private:
	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

public:
	///** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetCameraComponent() const { return CameraComp; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

/************************************************************************/
/* Movement and Rotation                                                */
/************************************************************************/
private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ControlSetting", meta = (AllowPrivateAccess = "true"))
	bool bUseMouse;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Debug", meta = (AllowPrivateAccess = "true"))
	FVector CurrentAimVector;

public:
	/** Called for forwards/backward input */
	virtual void MoveForward(float Value);

	/** Called for side to side input */
	virtual void MoveRight(float Value);

	void HandleInputRotation(FVector Direction);

/************************************************************************/
/* Editor                                                               */
/************************************************************************/
#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif
};