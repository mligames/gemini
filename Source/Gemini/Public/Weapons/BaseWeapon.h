// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseWeapon.generated.h"

class UStaticMeshComponent;
class UArrowComponent;
class AMechCharacter;
class UAmmoInventoryComponent;

UCLASS(ABSTRACT)
class GEMINI_API ABaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseWeapon(const class FObjectInitializer& ObjectInitializer);

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
	USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	float ShotsPerMinute;

	/* Bullet Spread in degrees Affected by Character Accuracy Stat*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin = 0.0f))
	float FireSpread;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UArrowComponent* MuzzlePoint;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName MuzzleMountSocket;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	bool bCanFire;

	AMechCharacter* MechOwner;

	UAmmoInventoryComponent* AmmoInventoryComp;

public:

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual UAmmoInventoryComponent* GetAmmoInventory();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void SetAmmoInventory(UAmmoInventoryComponent* ammoComp);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual AMechCharacter* GetMechOwner();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void SetMechOwner(AMechCharacter* mechOwner);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void FireWeapon();

};
