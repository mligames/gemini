// Fill out your copyright notice in the Description page of Project Settings.


#include "GeminiPlayerController.h"
#include "Engine/GameEngine.h"
#include "..\..\Public\Controllers\GeminiPlayerController.h"

AGeminiPlayerController::AGeminiPlayerController(const class FObjectInitializer& ObjectInitializer) : APlayerController(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;

	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}


void AGeminiPlayerController::Tick(float DeltaTime)
{
}

void AGeminiPlayerController::SetupInputComponent()
{
	APlayerController::SetupInputComponent();
}

void AGeminiPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	if (InPawn)
	{
		MechPawn = Cast<AMechCharacter>(InPawn);
	}
}

void AGeminiPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
}

AMechCharacter* AGeminiPlayerController::GetMechPawn() const
{
	if (MechPawn)
	{
		return MechPawn;
	}

	return NULL;
}
