// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Munitions/Munition.h"
#include "HitScanMunition.generated.h"

/**
 * 
 */
UCLASS()
class GEMINI_API AHitScanMunition : public AMunition
{
	GENERATED_BODY()

public:
	AHitScanMunition(const class FObjectInitializer& ObjectInitializer);

};
