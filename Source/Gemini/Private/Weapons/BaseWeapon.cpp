// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseWeapon.h"
#include "Engine/GameEngine.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "MechCharacter.h"

// Sets default values
ABaseWeapon::ABaseWeapon(const class FObjectInitializer& ObjectInitializer): AActor(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.

}

UAmmoInventoryComponent* ABaseWeapon::GetAmmoInventory()
{
	if (AmmoInventoryComp) 
	{
		return AmmoInventoryComp;
	}

	return nullptr;
}

void ABaseWeapon::SetAmmoInventory(UAmmoInventoryComponent* ammoComp)
{
	AmmoInventoryComp = ammoComp;
}

AMechCharacter* ABaseWeapon::GetMechOwner()
{
	if (MechOwner) 
	{
		return MechOwner;
	}
	return nullptr;
	
}

void ABaseWeapon::SetMechOwner(AMechCharacter* mechOwner)
{
	MechOwner = mechOwner;
}

// Called when the game starts or when spawned
void ABaseWeapon::BeginPlay()
{
	AActor::BeginPlay();
	
}

// Called every frame
void ABaseWeapon::Tick(float DeltaTime)
{
	AActor::Tick(DeltaTime);

}
void ABaseWeapon::FireWeapon()
{
	//if (GEngine)
	//{
	//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Fired Weapon!"));
	//}

}
