// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GeminiGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GEMINI_API AGeminiGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
