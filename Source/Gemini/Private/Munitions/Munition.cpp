// Fill out your copyright notice in the Description page of Project Settings.


#include "Munition.h"

// Sets default values
AMunition::AMunition(const class FObjectInitializer& ObjectInitializer) :AActor(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMunition::BeginPlay()
{
	AActor::BeginPlay();
	
}

// Called every frame
void AMunition::Tick(float DeltaTime)
{
	AActor::Tick(DeltaTime);

}

