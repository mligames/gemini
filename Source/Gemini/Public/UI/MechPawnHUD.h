// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MechPawnHUD.generated.h"

/**
 * 
 */
UCLASS()
class GEMINI_API AMechPawnHUD : public AHUD
{
	GENERATED_BODY()

public:
    AMechPawnHUD(const class FObjectInitializer& ObjectInitializer);

};
