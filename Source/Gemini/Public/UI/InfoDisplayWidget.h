// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InfoDisplayWidget.generated.h"

class AActor;
class UTextBlock;
class APlayerController;
/**
 * 
 */
UCLASS()
class GEMINI_API UInfoDisplayWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	// Sets default values for this character's properties
	UInfoDisplayWidget(const FObjectInitializer& ObjectInitializer);
	// Variable to hold the widget After Creating it.
	UPROPERTY(EditDefaultsOnly, Category = "Text Info")
	FText TextInfo;

	UFUNCTION(BlueprintCallable, Category = "Text Info")
	void SetInfoText(FText Value);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Text Info")
	FVector TextOffset;

	//UFUNCTION(BlueprintCallable, Category = "Text Info")
	//FVector GetTextDisplayLocation();

	AActor* InfoActor;

	UFUNCTION(BlueprintCallable, Category = "Text Info")
	AActor* GetInfoActor();

	//UFUNCTION(BlueprintCallable, Category = "Text Info")
	//void DisplayInfo(APlayerController* Value);
};
