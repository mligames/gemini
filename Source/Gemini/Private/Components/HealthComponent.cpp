// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "Engine/GameEngine.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent(const class FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...

}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	AActor* OwnerRef = GetOwner();
	if (OwnerRef)
	{
		OwnerRef->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::HandleTakeAnyDamage);
	}

}

void UHealthComponent::HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstegatedBy, AActor* DamageCauser)
{
	if (Damage <= 0.0f)
	{
		return;
	}

	CurrentHealth = FMath::Clamp(CurrentHealth - Damage, 0.0f, MaxHealth);
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Hit Health Comp!"));
	}
	//UE_LOG(LogTemp, Log, TEXT("Health Changed: %s"), *FString::SanitizeFloat(Health));

	OnHealthChanged.Broadcast(this, CurrentHealth, Damage, DamageType, InstegatedBy, DamageCauser);
}

bool UHealthComponent::IsAlive() const
{
	return CurrentHealth > 0;
}

bool UHealthComponent::IsDying() const
{
	return false;
}

bool UHealthComponent::IsDead() const
{
	return false;
}

float UHealthComponent::GetHealth() const
{
	return CurrentHealth;
}


