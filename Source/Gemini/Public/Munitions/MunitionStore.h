// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MunitionStore.generated.h"

UENUM(BlueprintType)
enum class EMunitionType : uint8
{
	Projectile_Small,
	Projectile_Medium,
	Projectile_Large,
	Accelerant,
	Capacitor,
	Explosive,
	Battery,
	Atomic,
	PlasmaCore
};

/**
 * 
 */
UCLASS()
class GEMINI_API UMunitionStore : public UObject
{
	GENERATED_BODY()
public:
	UMunitionStore(const class FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Munition")
	EMunitionType MunitionType;

private:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Munition", meta = (AllowPrivateAccess = "true"))
	float CurrentAmount;

public:

	UFUNCTION(BlueprintCallable, Category = "Munition")
	EMunitionType GetMunitionType();

	UFUNCTION(BlueprintCallable, Category = "Munition")
	float GetCurrentAmount() const;

	UFUNCTION(BlueprintCallable, Category = "Munition")
	void SetCurrentAmount(const float Value);

};
