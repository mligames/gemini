// Fill out your copyright notice in the Description page of Project Settings.


#include "HitScanWeapon.h"
#include "Engine/GameEngine.h"
#include "Kismet/GameplayStatics.h"
#include "HealthComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "DrawDebugHelpers.h"


AHitScanWeapon::AHitScanWeapon(const class FObjectInitializer& ObjectInitializer) :ABaseWeapon(ObjectInitializer) 
{
	PrimaryActorTick.bCanEverTick = true;
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->bCastDynamicShadow = true;
	WeaponMesh->CastShadow = true;
	RootComponent = WeaponMesh;

	MuzzleMountSocket = "Barrel_end";
	MuzzlePoint = CreateDefaultSubobject<UArrowComponent>(TEXT("MuzzlePoint"));
	MuzzlePoint->SetupAttachment(WeaponMesh);
	MuzzlePoint->AttachToComponent(WeaponMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, MuzzleMountSocket);
}

void AHitScanWeapon::FireWeapon()
{
	if (MuzzlePoint)
	{
		FVector TraceStart = MuzzlePoint->GetComponentLocation();
		FVector TraceDirection = MuzzlePoint->GetForwardVector();

		float HalfRad = FMath::DegreesToRadians(FireSpread);

		TraceDirection = FMath::VRandCone(TraceDirection, HalfRad, HalfRad);

		FVector TraceEnd = TraceStart + (TraceDirection * WeaponRange);

		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(GetOwner());
		QueryParams.AddIgnoredActor(this);
		QueryParams.bTraceComplex = true;
		QueryParams.bReturnPhysicalMaterial = true;

		FHitResult Hit;
		if (GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceEnd, ECC_Pawn, QueryParams))
		{
			//AActor* HitActor = Hit.GetActor();
			UHealthComponent* HealthComp;
			HealthComp = Hit.GetActor()->FindComponentByClass<UHealthComponent>();
			if (HealthComp)
			{
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Emerald, FString::Printf(TEXT("The Actor Being Hit is: %s"), *Hit.GetActor()->GetName()));
				}
			}
			// 20.0f is hard coded damage.
			UGameplayStatics::ApplyPointDamage(Hit.GetActor(), 20.0f, TraceDirection, Hit, GetOwner()->GetInstigatorController(), this, DamageType);
		}
		DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Red, false, 1.0f, 0, 10.0f);

	}

}