// Fill out your copyright notice in the Description page of Project Settings.


#include "MechDataComponent.h"
#include "MunitionStore.h"

// Sets default values for this component's properties
UMechDataComponent::UMechDataComponent(const class FObjectInitializer& ObjectInitializer) : UActorComponent(ObjectInitializer)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


float UMechDataComponent::GetBaseMovementAcceleration() const
{
	return BaseMovementAcceleration;
}

void UMechDataComponent::SetBaseMovementAcceleration(const float Value)
{
	BaseMovementAcceleration = Value;
}

float UMechDataComponent::GetBaseMovementSpeed() const
{
	return BaseMovementSpeed;
}

void UMechDataComponent::SetBaseMovementSpeed(const float Value)
{
	BaseMovementSpeed = Value;
}

float UMechDataComponent::GetBaseRotationSpeed() const
{
	return BaseRotationSpeed;
}

void UMechDataComponent::SetBaseRotationSpeed(const float Value)
{
	BaseRotationSpeed = Value;
}

float UMechDataComponent::GetMunitionStorageCapacity() const
{
	return 0.0f;
}

void UMechDataComponent::SetMunitionStorageCapacity(const float Value)
{
}

float UMechDataComponent::GetMunitionCount() const
{
	return 0.0f;
}

bool UMechDataComponent::HasAmmoType(UMunitionStore* Value)
{
	return false;
}


// Called when the game starts
void UMechDataComponent::BeginPlay()
{
	UActorComponent::BeginPlay();

	// ...
	
}


// Called every frame
void UMechDataComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	UActorComponent::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

