// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapons/BaseWeapon.h"
#include "HitScanWeapon.generated.h"

class UDamageType;

/**
 * 
 */
UCLASS()
class GEMINI_API AHitScanWeapon : public ABaseWeapon
{
	GENERATED_BODY()

public:
	AHitScanWeapon(const class FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	float WeaponRange;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<UDamageType> DamageType;

public:
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void FireWeapon() override;
};
