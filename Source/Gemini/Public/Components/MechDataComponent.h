// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MechDataComponent.generated.h"

class UMunitionStore;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GEMINI_API UMechDataComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMechDataComponent(const class FObjectInitializer& ObjectInitializer);

/************************************************************************/
/* Acceleration                                                         */
/************************************************************************/
private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "MovementStats", meta = (AllowPrivateAccess = "true"))
	float BaseMovementAcceleration;

public:
	UFUNCTION(BlueprintCallable, Category = "MovementStats")
	float GetBaseMovementAcceleration() const;

	UFUNCTION(BlueprintCallable, Category = "MovementStats")
	void SetBaseMovementAcceleration(const float Value);

/************************************************************************/
/* Movement Speed                                                       */
/************************************************************************/
private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "MovementStats", meta = (AllowPrivateAccess = "true"))
	float BaseMovementSpeed;
public:
	UFUNCTION(BlueprintCallable, Category = "MovementStats")
	float GetBaseMovementSpeed() const;

	UFUNCTION(BlueprintCallable, Category = "MovementStats")
	void SetBaseMovementSpeed(const float Value);
/************************************************************************/
/* Rotation Speed                                                       */
/************************************************************************/
private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "MovementStats", meta = (AllowPrivateAccess = "true"))
	float BaseRotationSpeed;
public:
	UFUNCTION(BlueprintCallable, Category = "MovementStats")
	float GetBaseRotationSpeed() const;

	UFUNCTION(BlueprintCallable, Category = "MovementStats")
	void SetBaseRotationSpeed(const float Value);

/************************************************************************/
/* Munition Storage                                                     */
/************************************************************************/
private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Munitions", meta = (AllowPrivateAccess = "true"))
	float MunitionStorageCapacity;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Munitions", meta = (AllowPrivateAccess = "true"))
	TArray<UMunitionStore*> MunitionStorage;

public:
	UFUNCTION(BlueprintCallable, Category = "Munitions")
	float GetMunitionStorageCapacity() const;

	UFUNCTION(BlueprintCallable, Category = "Munitions")
	void SetMunitionStorageCapacity(const float Value);

	UFUNCTION(BlueprintCallable, Category = "MovementStats")
	float GetMunitionCount() const;

	UFUNCTION(BlueprintCallable, Category = "MovementStats")
	bool HasAmmoType(UMunitionStore* Value);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
