// Fill out your copyright notice in the Description page of Project Settings.


#include "MechCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/PlayerController.h"
#include "GeminiPlayerController.h"
#include "MechDataComponent.h"
#include "BaseWeapon.h"
#include "AmmoInventoryComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/ArrowComponent.h"
#include "Engine/GameEngine.h"

const FName AMechCharacter::MoveForwardBinding("MoveForward");
const FName AMechCharacter::MoveRightBinding("MoveRight");
const FName AMechCharacter::LookForwardBinding("LookForward");
const FName AMechCharacter::LookRightBinding("LookRight");

AMechCharacter::AMechCharacter(const FObjectInitializer& ObjectInitializer): ACharacter(ObjectInitializer)
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetMesh()->SetRelativeLocation(FVector(0.0f, 0.0f, -250.0f));
	GetMesh()->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(GetRootComponent());
	CameraBoom->bAbsoluteRotation = true;
	CameraBoom->bUsePawnControlRotation = false;
	//CameraSpringArm->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
	//CameraBoom->TargetArmLength = 1200.0f;

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComp->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComp->bUsePawnControlRotation = false;

	MechBodyMountSocket = "Mount_Top";
	MechDataComp = CreateDefaultSubobject<UMechDataComponent>(TEXT("MechStats"));
	AmmoInventoryComp = CreateDefaultSubobject<UAmmoInventoryComponent>(TEXT("AmmoInventory"));
	MechBodyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MechBody"));
	MechBodyMesh->SetupAttachment(GetMesh(), MechBodyMountSocket);

	WeaponLeftSocket = "Mount_Weapon_L";
	WeaponRightSocket = "Mount_Weapon_R";
}

void AMechCharacter::BeginPlay()
{
	Super::BeginPlay();

	FActorSpawnParameters WeaponSpawnParams;
	WeaponSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	WeaponLeft = GetWorld()->SpawnActor<ABaseWeapon>(SelectedWeaponLeft, FVector::ZeroVector, FRotator::ZeroRotator, WeaponSpawnParams);
	if (WeaponLeft && MechBodyMesh)
	{
		WeaponLeft->AttachToComponent(MechBodyMesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponLeftSocket);
		WeaponLeft->SetOwner(GetOwner());
		//if (GEngine)
		//{
		//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Emerald, TEXT("Left Weapon Mech Owner " + WeaponLeft->GetMechOwner()->GetName()));
		//}
	}

	WeaponRight = GetWorld()->SpawnActor<ABaseWeapon>(SelectedWeaponRight, FVector::ZeroVector, FRotator::ZeroRotator, WeaponSpawnParams);
	if (WeaponRight && MechBodyMesh)
	{
		WeaponRight->AttachToComponent(MechBodyMesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponRightSocket);
		WeaponRight->SetOwner(GetOwner());
		WeaponRight->SetMechOwner(this);
		//if (GEngine)
		//{
		//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Emerald, TEXT("Right Weapon Mech Owner " + WeaponRight->GetMechOwner()->GetName()));
		//}
	}

	if (GEngine)
	{
		if (bUseMouse) 
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Orange, TEXT("Mouse Aim ENABLED"));
		}
		else 
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Orange, TEXT("Mouse Aim DISABLED"));
		}

	}
}

void AMechCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	const float LookForwardValue = GetInputAxisValue(LookForwardBinding);
	const float LookRightValue = GetInputAxisValue(LookRightBinding);
	const FVector LookDirection = FVector(LookForwardValue, LookRightValue, 0.f);
	HandleInputRotation(LookDirection);
}

void AMechCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMechCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMechCharacter::MoveRight);
	PlayerInputComponent->BindAxis(LookForwardBinding);
	PlayerInputComponent->BindAxis(LookRightBinding);

	PlayerInputComponent->BindAction("FireWeaponLeft", IE_Pressed, this, &AMechCharacter::FireWeaponLeft);
	PlayerInputComponent->BindAction("FireWeaponLeft", IE_Released, this, &AMechCharacter::StopFireWeaponLeft);

	PlayerInputComponent->BindAction("FireWeaponRight", IE_Pressed, this, &AMechCharacter::FireWeaponRight);
	PlayerInputComponent->BindAction("FireWeaponRight", IE_Released, this, &AMechCharacter::StopFireWeaponRight);

}

void AMechCharacter::FireWeaponLeft()
{
	if (WeaponLeft) 
	{
		WeaponLeft->FireWeapon();
	}
}

void AMechCharacter::StopFireWeaponLeft()
{

}

void AMechCharacter::FireWeaponRight()
{
	if (WeaponRight)
	{
		WeaponRight->FireWeapon();
	}
}

void AMechCharacter::StopFireWeaponRight()
{
}

void AMechCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(FVector::ForwardVector, Value);
	}
}

void AMechCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(FVector::RightVector, Value);
	}
}

void AMechCharacter::HandleInputRotation(FVector Direction)
{
	if (bUseMouse) 
	{
		AGeminiPlayerController* PlayerController = Cast<AGeminiPlayerController>(GetController());
		if (PlayerController != nullptr)
		{
			FHitResult TraceHitResult;

			PlayerController->GetHitResultUnderCursor(ECC_Camera, true, TraceHitResult);

			FRotator lookRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), TraceHitResult.Location);

			FRotator rot = FMath::RInterpTo(GetActorRotation(), lookRotation, GetWorld()->GetDeltaSeconds(), 180.0f);

			PlayerController->SetControlRotation(rot);
		}

	}
	else 
	{
		if (Direction.SizeSquared() > 0.0f)
		{
			float rotationRate = 180.0f;
			if (MechDataComp) 
			{
				rotationRate = MechDataComp->GetBaseRotationSpeed();
			}
			FRotator rot = FMath::RInterpTo(GetActorRotation(), Direction.Rotation(), GetWorld()->GetDeltaSeconds(), rotationRate);

			GetController()->SetControlRotation(rot);
			// Spawn projectile at an offset from this pawn
			//const FVector SpawnLocation = GetActorLocation() + FireRotation.RotateVector(GunOffset);

		}
	}

	//if (GEngine) 
	//{
	//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Emerald, TEXT("Current Aim Vector " + Direction.ToString()));
	//}

	//CurrentAimVector = Direction;

	//if (Direction.SizeSquared() > 0.0f) {}
	AGeminiPlayerController* PlayerController = Cast<AGeminiPlayerController>(GetController());
	if (PlayerController != nullptr)
	{


	}

}

void AMechCharacter::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
