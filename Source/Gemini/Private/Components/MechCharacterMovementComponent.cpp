// Fill out your copyright notice in the Description page of Project Settings.


#include "MechCharacterMovementComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"

UMechCharacterMovementComponent::UMechCharacterMovementComponent(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

//============================================================================================
//Replication
//============================================================================================
void UMechCharacterMovementComponent::UpdateFromCompressedFlags(uint8 Flags)
{
	Super::UpdateFromCompressedFlags(Flags);
	//The Flags parameter contains the compressed input flags that are stored in the saved move.
	//UpdateFromCompressed flags simply copies the flags from the saved move into the movement component.
	//It basically just resets the movement component to the state when the move was made so it can simulate from there.

	//TODO: Add other Wants
}

uint8 UMechCharacterMovementComponent::FSavedMove_MechCharacterMovement::GetCompressedFlags() const
{
	uint8 Result = Super::GetCompressedFlags();


	return Result;
}

class FNetworkPredictionData_Client* UMechCharacterMovementComponent::GetPredictionData_Client() const
{
	check(PawnOwner != NULL);
	check(PawnOwner->Role < ROLE_Authority);

	if (!ClientPredictionData)
	{
		UMechCharacterMovementComponent* MutableThis = const_cast<UMechCharacterMovementComponent*>(this);

		MutableThis->ClientPredictionData = new FNetworkPredictionData_Client_MechCharacterMovement(*this);
		MutableThis->ClientPredictionData->MaxSmoothNetUpdateDist = 92.f;
		MutableThis->ClientPredictionData->NoSmoothNetUpdateDist = 140.f;
	}

	return ClientPredictionData;
}

void UMechCharacterMovementComponent::FSavedMove_MechCharacterMovement::Clear()
{
	Super::Clear();
	SavedMoveDirection = FVector::ZeroVector;
}

void UMechCharacterMovementComponent::OnMovementUpdated(float DeltaTime, const FVector& OldLocation, const FVector& OldVelocity)
{
	Super::OnMovementUpdated(DeltaTime, OldLocation, OldVelocity);

	if (!CharacterOwner)
	{
		return;
	}

	//Store movement vector
	if (PawnOwner->IsLocallyControlled())
	{
		MoveDirection = PawnOwner->GetLastMovementInputVector();
	}
	//Send movement vector to server
	//if (PawnOwner->Role < ROLE_Authority)
	//{
	//	ServerSetMoveDirection(MoveDirection);
	//}

}

void UMechCharacterMovementComponent::FSavedMove_MechCharacterMovement::SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, FNetworkPredictionData_Client_Character& ClientData)
{
	Super::SetMoveFor(Character, InDeltaTime, NewAccel, ClientData);

	UMechCharacterMovementComponent* CharacterMovement = Cast<UMechCharacterMovementComponent>(Character->GetCharacterMovement());
	if (CharacterMovement)
	{
		//bSavedWantsToCasuallyWalk = CharacterMovement->bWantsToCasuallyWalk;
		//bSavedWantsToSprint = CharacterMovement->bWantsToSprint;
		//bSavedWantsToAimDownSight = CharacterMovement->bWantsToCasuallyWalk;

	}
}

void UMechCharacterMovementComponent::FSavedMove_MechCharacterMovement::PrepMoveFor(ACharacter* Character)
{
	Super::PrepMoveFor(Character);

	UMechCharacterMovementComponent* CharacterMovement = Cast<UMechCharacterMovementComponent>(Character->GetCharacterMovement());
	if (CharacterMovement)
	{
		//This is just the exact opposite of SetMoveFor. It copies the state from the saved move to the movement
		//component before a correction is made to a client.
		CharacterMovement->MoveDirection = SavedMoveDirection;
		//Don't update flags here. They're automatically setup before corrections using the compressed flag methods.
	}
}

bool UMechCharacterMovementComponent::FSavedMove_MechCharacterMovement::CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const
{
	//Set which moves can be combined together. This will depend on the bit flags that are used.	
	//This pretty much just tells the engine if it can optimize by combining saved moves. There doesn't appear to be
	//any problem with leaving it out, but it seems that it's good practice to implement this anyways.

	return Super::CanCombineWith(NewMove, Character, MaxDelta);
}


float UMechCharacterMovementComponent::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();
	
	// Modify Speed here

	return MaxSpeed;
}

float UMechCharacterMovementComponent::GetMaxAcceleration() const
{
	float MaxAccel = Super::GetMaxAcceleration();

	// Modify Movement Acceleration here

	return MaxAccel;
}

float UMechCharacterMovementComponent::GetMoveVelocity() const
{
	return Super::GetLastUpdateVelocity().Size();
}

float UMechCharacterMovementComponent::GetDirection() const
{
	FRotator controlRotation = Super::GetLastUpdateRotation();
	FRotator pawnRotation = Super::GetLastUpdateVelocity().Rotation();

	FRotator delta = UKismetMathLibrary::NormalizedDeltaRotator(pawnRotation, controlRotation);

	return delta.Yaw;
}

bool UMechCharacterMovementComponent::IsMovingForward() const
{
	if (!PawnOwner)
	{
		return false;
	}

	FVector Forward = PawnOwner->GetActorForwardVector();
	FVector MoveDirection = Velocity.GetSafeNormal();

	//Ignore vertical movement
	Forward.Z = 0.0f;
	MoveDirection.Z = 0.0f;

	float VelocityDot = FVector::DotProduct(Forward, MoveDirection);
	return VelocityDot > 0.7f;//Check to make sure difference between headings is not too great.
}


bool UMechCharacterMovementComponent::HandlePendingLaunch()
{
	if (!PendingLaunchVelocity.IsZero() && HasValidData())
	{
		Velocity = PendingLaunchVelocity;
		//Remmed out so our dodge move won't play the falling animation.
		//SetMovementMode(MOVE_Falling);
		PendingLaunchVelocity = FVector::ZeroVector;
		bForceNextFloorCheck = true;
		return true;
	}

	return false;
}

UMechCharacterMovementComponent::FNetworkPredictionData_Client_MechCharacterMovement::FNetworkPredictionData_Client_MechCharacterMovement(const UMechCharacterMovementComponent& ClientMovement) :Super(ClientMovement)
{
}

FSavedMovePtr UMechCharacterMovementComponent::FNetworkPredictionData_Client_MechCharacterMovement::AllocateNewMove()
{
	return FSavedMovePtr(new FSavedMove_MechCharacterMovement());
}


