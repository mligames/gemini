// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AmmoInventoryComponent.generated.h"

class UMunitionStore;
//enum class EMunitionType;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GEMINI_API UAmmoInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAmmoInventoryComponent(const class FObjectInitializer& ObjectInitializer);

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Munitions")
	//TArray<UMunitionStore*> MunitionList;

	UPROPERTY(EditDefaultsOnly, Category = "Munitions")
	int MaxCapacity;

	UPROPERTY(EditDefaultsOnly, Category = "Munitions")
	int CurrentCapacity;

	UPROPERTY(EditDefaultsOnly, Category = "Munitions")
	TMap<int, UMunitionStore*> MunitionMap;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void InitMunitionStorage();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void AddMunitionType(int munitionType, int amount);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	bool TryGetMunition(int munitionType, int amount);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	int GetRemainingCapacity();

};
